package ar.uba.fi.tdd.exercise;

public class ItemAgesTerribly implements ItemQualityType {
    public static final String conjured = "Conjured";
    private static final int terribleQualityToDecrease = 2;

    @Override
    public boolean isMyTypeOfItem(String itemName) {
        return (itemName.equals(conjured));
    }

    @Override
    public void updateQuality(Item item) {
        item.quality = Math.max(item.quality - terribleQualityToDecrease, 0);
    }

    @Override
    public void updateSellIn(Item item) {
        ItemSellinManager.growItemOld(item);
    }
}
