package ar.uba.fi.tdd.exercise;

public class ItemAgesGracefully implements ItemQualityType {
    private static final int gracefulQualityToIncrease = 1;

    public static final String agedBrie = "Aged Brie";

    @Override
    public boolean isMyTypeOfItem(String itemName) {
        return (itemName.equals(agedBrie));
    }

    @Override
    public void updateQuality(Item item) {
        item.quality += (ItemQualityManager.isAtItsMaxQuality(item)) ? 0 : gracefulQualityToIncrease;
    }

    @Override
    public void updateSellIn(Item item) {
        ItemSellinManager.growItemOld(item);
    }
}
