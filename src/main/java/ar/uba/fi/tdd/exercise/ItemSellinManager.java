package ar.uba.fi.tdd.exercise;

public class ItemSellinManager {
    public static void growItemOld(Item item){
        item.sellIn = (item.sellIn > 0) ? item.sellIn - 1 : item.sellIn;
    }

    public static boolean isTheItemExpired(Item item) { return (item.sellIn <= 0);}

    public static boolean isThisItemSellinSmallerOrEqualThan(Item item, int date) {
        return (item.sellIn <= date);
    }

    public static boolean isThisItemSellinGreaterThan(Item item, int date) {
        return (date < item.sellIn);
    }
}
