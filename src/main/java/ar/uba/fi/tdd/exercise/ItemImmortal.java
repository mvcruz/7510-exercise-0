package ar.uba.fi.tdd.exercise;

public class ItemImmortal implements ItemQualityType {
    public static final String Sulfuras = "Sulfuras";

    @Override
    public boolean isMyTypeOfItem(String itemName) {
        return itemName.contains(Sulfuras);
    }

    @Override
    public void updateQuality(Item item) {

    }

    @Override
    public void updateSellIn(Item item) {

    }

}
