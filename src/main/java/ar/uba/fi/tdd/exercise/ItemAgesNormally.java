package ar.uba.fi.tdd.exercise;

public class ItemAgesNormally implements ItemQualityType {
    private static final int normalQualityToDecrease = 1;
    private static final int expiredQualityToDecrease = 2;

    @Override
    public boolean isMyTypeOfItem(String itemName) {
        return true;
    }

    @Override
    public void updateQuality(Item item) {
        int toDecrease = (ItemSellinManager.isTheItemExpired(item)) ? expiredQualityToDecrease : normalQualityToDecrease;
        item.quality = Math.max(item.quality - toDecrease, 0);
    }

    @Override
    public void updateSellIn(Item item) {
        ItemSellinManager.growItemOld(item);
    }
}
