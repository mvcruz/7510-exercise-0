
package ar.uba.fi.tdd.exercise;

class GildedRose {
  Item[] items;
  ItemManager itemManager;

    public GildedRose(Item[] _items) {
        itemManager = new ItemManager();
        items = _items;
    }

    public void updateQuality() {
        for (Item item : items) {
            itemManager.update(item);
        }
    }
}
