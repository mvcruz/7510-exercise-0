package ar.uba.fi.tdd.exercise;

public interface ItemQualityType {
    boolean isMyTypeOfItem(String itemName);

    void updateQuality(Item item);
    void updateSellIn(Item item);
}
