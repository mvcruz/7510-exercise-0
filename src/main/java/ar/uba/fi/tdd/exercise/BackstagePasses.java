package ar.uba.fi.tdd.exercise;

public class BackstagePasses extends ItemAgesGracefully{
    public static final String backstagePasses = "Backstage passes";
    private static final int maxSellinToIncreaseQualityNormally = 10;
    private static final int maxSellinToIncreaseQualityTwice = 5;
    private static final int bpNormalAmountToIncrease = 1;
    private static final int bpFasterAmountToIncrease = 2;

    @Override
    public boolean isMyTypeOfItem(String itemName) {
        return itemName.contains(backstagePasses);
    }

    @Override
    public void updateQuality(Item item) {
        super.updateQuality(item);
        if(ItemQualityManager.isAtItsMaxQuality(item) || ItemSellinManager.isThisItemSellinGreaterThan(item, maxSellinToIncreaseQualityNormally))
            return;

        if(ItemSellinManager.isTheItemExpired(item)) {
            item.quality = 0;
            return;
        }

        item.quality += (ItemSellinManager.isThisItemSellinSmallerOrEqualThan(item, maxSellinToIncreaseQualityTwice)) ? bpFasterAmountToIncrease : bpNormalAmountToIncrease;
    }
}
