package ar.uba.fi.tdd.exercise;

public class ItemManager {
    ItemQualityManager qualityManager;
    ItemSellinManager sellinManager;

    public ItemManager() {
        qualityManager = new ItemQualityManager();
        sellinManager = new ItemSellinManager();
    }

    public void update(Item item){
        qualityManager.getItemQualityType(item).updateQuality(item);
        qualityManager.getItemQualityType(item).updateSellIn(item);
    }
}
