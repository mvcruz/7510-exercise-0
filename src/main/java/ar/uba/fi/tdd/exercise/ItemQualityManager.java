package ar.uba.fi.tdd.exercise;

import java.util.ArrayList;
import java.util.List;

public class ItemQualityManager {
    public ItemAgesNormally normalItem;
    public static List<ItemQualityType> specialItemTypes;

    private static final int maxQuality = 50;

    public ItemQualityManager() {
        normalItem = new ItemAgesNormally();
        specialItemTypes = new ArrayList<>();

        ItemImmortal itemImmortal = new ItemImmortal();
        ItemAgesGracefully itemAgesGracefully = new ItemAgesGracefully();
        BackstagePasses backstagePasses = new BackstagePasses();
        ItemAgesTerribly conjured = new ItemAgesTerribly();

        specialItemTypes.add(itemImmortal);
        specialItemTypes.add(itemAgesGracefully);
        specialItemTypes.add(backstagePasses);
        specialItemTypes.add(conjured);
    }

    public static boolean isAtItsMaxQuality(Item item) {
        return (item.quality == maxQuality);
    }

    public ItemQualityType getItemQualityType(Item item){
        for(ItemQualityType specialItem: specialItemTypes) {
            if(specialItem.isMyTypeOfItem(item.Name)) {
                return specialItem;
            }
        }
        return normalItem;
    }
}
