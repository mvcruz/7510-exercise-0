package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class GildedRoseTest {

	@Test
	@DisplayName("Legendary items don't change their quality over time")
	void updateLegendaryItemAndCheckQuality() {
		Item sulfura = new Item("Sulfuras, Hand of Ragnaros", 4, 3);
		Item[] items = {sulfura};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(3, sulfura.quality);
	}

	@Test
	@DisplayName("Legendary items don't change their sellIn date over time")
	void updateLegendaryItemAndCheckSellIn() {
		Item sulfura = new Item("Sulfuras, Hand of Ragnaros", 4, 3);
		Item[] items = {sulfura};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(4, sulfura.sellIn);
	}

	@Test
	@DisplayName("Normal items decrease their quality by one before sellIn date is reached")
	void updateNormalItemBeforeSellInAndCheckQuality() {
		Item normal = new Item("Normal", 2, 6);
		Item[] items = {normal};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(5, normal.quality);
	}

	@Test
	@DisplayName("Normal items decrease their quality by two when sellIn date is reached")
	void updateNormalItemWhenSellInAndCheckQuality() {
		Item normal = new Item("Normal", 2, 6);
		Item[] items = {normal};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertEquals(2, normal.quality);
	}

	@Test
	@DisplayName("Normal item's quality is always 0 or more")
	void updateNormalItemAndQualityIsZeroOrMore() {
		Item normal = new Item("Normal", 2, 1);
		Item[] items = {normal};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertEquals(0, normal.quality);
	}

	@Test
	@DisplayName("Normal items change their sellIn date over time, 1 decrease per update")
	void updateNormalItemAndCheckSellIn() {
		Item normal = new Item("Normal", 2, 6);
		Item[] items = {normal};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(1, normal.sellIn);
	}

	@Test
	@DisplayName("Aged Brie Item increase its quality by 1 per update")
	void updateAgedBrieItemAndCheckQuality() {
		Item brie = new Item("Aged Brie", 2, 6);
		Item[] items = {brie};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(7, brie.quality);
	}


	@Test
	@DisplayName("Aged Brie item's quality is always less or equal than 50")
	void updateAgedBrieItemAndQualityIsFiftyOrLess() {
		Item brie = new Item("Aged Brie", 3, 49);
		Item[] items = {brie};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		app.updateQuality();
		assertEquals(50, brie.quality);
	}

	@Test
	@DisplayName("Aged Brie items change their sellIn date over time, 1 decrease per update")
	void updateAgedBrieItemAndCheckSellIn() {
		Item brie = new Item("Aged Brie", 2, 6);
		Item[] items = {brie};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(1, brie.sellIn);
	}

	@Test
	@DisplayName("BackstagePasses increase its quality by 1 per update when sellIn is 11 or more")
	void updateBackstagePassesBeforeSellInReaches10AndCheckQuality() {
		Item backstagePass = new Item("Backstage passes to a TAFKAL80ETC concert", 12, 6);
		Item[] items = {backstagePass};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(7, backstagePass.quality);
	}

	@Test
	@DisplayName("BackstagePasses increase its quality by 2 per update when sellIn is between 5 and 10")
	void updateBackstagePassesBeforeSellInReaches5AndCheckQuality() {
		Item backstagePass = new Item("Backstage passes to a TAFKAL80ETC concert", 7, 6);
		Item[] items = {backstagePass};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(8, backstagePass.quality);
	}

	@Test
	@DisplayName("BackstagePasses increase its quality by 3 per update when sellIn is between 1 and 5")
	void updateBackstagePassesBeforeSellInReaches0AndCheckQuality() {
		Item backstagePass = new Item("Backstage passes to a TAFKAL80ETC concert", 3, 6);
		Item[] items = {backstagePass};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(9, backstagePass.quality);
	}

	@Test
	@DisplayName("Backstage Passes drops its quality to 0 when sellIn reaches 0")
	void updateBackstagePassesWhenSellInReachesZeroAndCheckQuality() {
		Item backstagePass = new Item("Backstage passes to a TAFKAL80ETC concert", 1, 6);
		Item[] items = {backstagePass};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		app.updateQuality();
		assertEquals(0, backstagePass.quality);
	}

	@Test
	@DisplayName("Backstage Passes item's quality is always less or equal than 50")
	void updateBackstagePassItemAndQualityIsFiftyOrLess() {
		Item backstagePass = new Item("Backstage passes to a TAFKAL80ETC concert", 3, 49);
		Item[] items = {backstagePass};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(50, backstagePass.quality);
	}

	@Test
	@DisplayName("Backstage Passes  items change their sellIn date over time, 1 decrease per update")
	void updateBackstagePassItemAndCheckSellIn() {
		Item backstagePass = new Item("Backstage passes to a TAFKAL80ETC concert", 2, 6);
		Item[] items = {backstagePass};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(1, backstagePass.sellIn);
	}

	@Test
	@DisplayName("Conjured items decrease their quality by two over time")
	void updateConjuredItemAndCheckQuality() {
		Item conjured = new Item("Conjured", 3, 6);
		Item[] items = {conjured};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(4, conjured.quality);
	}

	@Test
	@DisplayName("Conjured items always have a quality of zero or more")
	void updateConjuredItemAndQualityIsZeroOrMore() {
		Item conjured = new Item("Conjured", 3, 1);
		Item[] items = {conjured};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(0, conjured.quality);
	}

	@Test
	@DisplayName("Conjured items change their sellIn date over time, 1 decrease per update")
	void updateConjuredItemAndCheckSellIn() {
		Item conjured = new Item("Conjured", 2, 6);
		Item[] items = {conjured};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertEquals(1, conjured.sellIn);
	}
}
